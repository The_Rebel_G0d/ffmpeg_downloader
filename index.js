const util = require("util");
const _ = require("lodash");
const mkdirp = require("mkdirp");
const requestP = require("request-promise-native");
const request = require("request");
const os = require("os");
const path = require("path");
const yauzl = require("yauzl");
const fs = require("fs");

let i = 0;

module.exports.download = (options = {}) => {
    return new Promise(function(resolve, reject) {
        let plat = ``;
        let arch = ``;
        let out = `./bin/ffmpeg/`;
        let components = ["ffmpeg"];
        let statusListener = function() {};
        let infoUpdater = function(info) {};

        if (util.isString(options.platform)) plat = options.platform;
        if (util.isString(options.arch)) arch = options.arch;
        if (util.isString(options.output)) out = options.output;
        if (util.isArray(options.components)) components = options.components;
        if (util.isString(options.components)) components = [options.components];
        if (util.isFunction(options.statusListener)) statusListener = options.statusListener;
        if (util.isFunction(options.infoUpdater)) infoUpdater = options.infoUpdater;

        if (plat.length == 0) {
            switch (os.type().toLowerCase().trim()) {
                case "windows_nt":
                    plat = "windows";
                    break;
                case "darwin":
                    plat = "osx";
                    break;
                default:
                    plat = "linux";
                    break;
            }
        }
        if (arch.length == 0) {
            arch = os.arch();
            if (arch[0] == "x") arch = arch.substr(1);
            if (arch.toLowerCase().trim().includes("arm")) arch = "armel";
            if (plat.toLowerCase().trim() == "osx") arch = "64";
        }
        if (out.length == 0) out = "./";
        if (out[out.length - 1] != "/") out = `${out}/`;
        //debugListener(`Downloading ffmpeg for ${plat}-${arch} to ${out}`);
        infoUpdater("Download required, initializing downloader...");

        mkdirp.sync(out);

        requestP({
            url: "http://ffbinaries.com/api/v1/version/latest",
            headers: {
                "User-Agent": `ffmpeg_downloader/${require(`${__dirname}/package.json`).version || "0.0.1"} rebel@trg0d.me`
            }
        }).then(data => {
            try {
                let resp = JSON.parse(data);
                if (util.isObject(resp.bin)) {
                    if (util.isObject(resp.bin[`${plat}-${arch}`])) {
                        let ourBin = resp.bin[`${plat}-${arch}`];
                        let tasks = [];
                        _.each(components, component => {
                            if (util.isString(ourBin[component])) {
                                (url => {
                                    tasks.push(new Promise(function(resolve, reject) {
                                        (payload => {
                                            infoUpdater(`Downloading ${component}`);
                                            request(url).on("response", res => {
                                                let bar = require("status-bar").create({total: res.headers["content-length"]}).on("render", arg => statusListener({
                                                    component,
                                                    numComponents: components.length,
                                                    barArgs: arg
                                                }));
                                                let ws = require("fs").createWriteStream(payload);
                                                ws.on("close", () => {
                                                    infoUpdater(`${component} download done, extracting...`);
                                                    yauzl.open(payload, {lazyEntries: true}, (err, zipfile) => {
                                                        if (err) {
                                                            reject(err);
                                                            return;
                                                        }
                                                        zipfile.readEntry();
                                                        zipfile.on("entry", entry => {
                                                            if (/\/$/.test(entry.fileName)) {
                                                                zipfile.readEntry();
                                                            } else {
                                                                if (!entry.fileName.includes("__MACOSX/")) {
                                                                    zipfile.openReadStream(entry, (err, stream) => {
                                                                        if (!err) {
                                                                            stream.pipe(fs.createWriteStream(`${out}${entry.fileName}`));
                                                                            stream.on("end", () => zipfile.readEntry());
                                                                        } else {
                                                                            //debugUpdater("Couldn't read a file", err, zipfile);
                                                                        }
                                                                    });
                                                                } else {
                                                                    zipfile.readEntry();
                                                                }
                                                            }
                                                        });
                                                        zipfile.on("end", () => {
                                                            fs.unlink(payload, err => {
                                                                //if (!err) debugUpdater("Unlinked payload:", payload); else debugUpdater("Failed to unlink payload", payload, err);
                                                                resolve();
                                                            });
                                                        });
                                                    });
                                                });
                                                res.pipe(bar);
                                                res.pipe(ws);
                                            });
                                        })(`${out}payload${++i}.zip`);
                                    }));
                                })(ourBin[component]);
                            }
                        });
                        Promise.all(tasks).then(() => {
                            infoUpdater("All download(s) complete");
                            resolve({plat, arch, out, components});
                        }).catch(reject);
                    } else {
                        reject(new Error("Invalid plat/arch"));
                    }
                } else {
                    reject(new Error("API error, resp.bin != object"));
                }
            } catch (e) {
                reject(e);
            }
        }).catch(reject);
    });
};